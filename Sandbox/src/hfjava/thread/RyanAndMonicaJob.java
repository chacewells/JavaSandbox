package hfjava.thread;

public class RyanAndMonicaJob implements Runnable {
	
	class BankAccount {
		private int balance = 100;
		public int getBalance() {return balance;}
		public void withdraw(int amt) {balance -= amt;}
	}
	
	private BankAccount account = new BankAccount();
	
	private synchronized void makeWithdrawal(int amt) {
		String tName = Thread.currentThread().getName();
		if (account.getBalance() >= amt) {
			System.out.println(tName + " is about to withdraw");
			try {
				System.out.println(tName + " is going to sleep");
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(tName + " woke up.");
			account.withdraw(amt);
			System.out.println(tName + " completes the withdrawal");
		} else {
			System.out.println("Sorry, not enough for " + tName);
		}
	}

	@Override
	public void run() {
		for (int x = 0; x < 10; x++) {
			makeWithdrawal(10);
			if (account.getBalance() < 0)
				System.out.println("Overdrawn!");
		}
	}

	public static void main(String[] args) {
		RyanAndMonicaJob job = new RyanAndMonicaJob();
		Thread one = new Thread(job);
		Thread two = new Thread(job);
		one.setName("Ryan");
		two.setName("Monica");
		one.start();
		two.start();
	}

}
