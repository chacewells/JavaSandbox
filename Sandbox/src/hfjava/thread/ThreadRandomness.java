package hfjava.thread;

public class ThreadRandomness {

	public static void main(String[] args) {
		Thread u = new Thread(nRun);
		u.start();
		System.out.println("main thread");
	}
	
	static Runnable mRun = () -> System.out.println("top of stack");

	static Runnable nRun = () -> {
		Thread t = new Thread(mRun);
		t.start();
	};

}
