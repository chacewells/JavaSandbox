package hfjava.thread;

public class ThreadNames {

	public static void main(String[] args) {
		MyRunnable mr = new MyRunnable();
		Thread a = new Thread(mr);
		Thread b = new Thread(mr);
		a.setName("Alpha");
		b.setName("Beta");
		a.start();
		b.start();
	}
	
	static class MyRunnable implements Runnable {

		@Override
		public void run() {
			for (int i : new int[25]) {
				System.out.println(Thread.currentThread().getName());
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}
