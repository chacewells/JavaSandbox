package com.wellsfromwales.sandbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class TryLambdas {
	
	static BufferedReader reader;
	static MockDB db;

	public static void main(String[] args) throws IOException {
		
		do {
			String s = askForFirst();
			showResults(s);
		} while (willContinue());
		
		System.out.println("c ya!");
		
		if ( reader != null )
			reader.close();
	}
	
	public static String askForFirst() throws IOException {
		if (reader == null)
			reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("What does the name begin with? ");
		String line = reader.readLine();
		return line;
	}
	
	static void showResults( String s ) {
		Predicate<String> p = q -> q.toUpperCase().startsWith(s.toUpperCase());
		if (db == null) db = new MockDB();

		List<String> names = db.getNames(p);
		if ( names.isEmpty() ) {
			System.out.println("I couldn't find any names that start with '"+s+"'");
		} else {
			System.out.println("These are the names beginning with '"+s+"'");
			printBorder();
			names
			.stream()
			.forEach(n->System.out.println(n));
		}
	}
	
	static void printBorder() {
		for ( int i=0; i++ < 40; System.out.print('*') );
		System.out.println();
	}
	
	static boolean willContinue() throws IOException {
		Predicate<String> p = s -> s.equalsIgnoreCase("yes") || s.equalsIgnoreCase("no");
		Predicate<String> q = r -> r.equalsIgnoreCase("yes");
		String answer;
		do {
			System.out.print("Another name? ");
			answer = reader.readLine();
		} while ( !p.test(answer) );
		
		return q.test(answer);
	}
	
	private static class MockDB {
		public static final String[] NAMES = {
			"Aaron",
			"Adam",
			"Aiden",
			"Alex",
			"Alexander",
			"Andrew",
			"Anthony",
			"Austin",
			"Benjamin",
			"Blake",
			"Brandon",
			"Brian",
			"Christopher",
			"Daniel",
			"David",
			"Dylan",
			"Ethan",
			"Harry",
			"Ian",
			"Isaac",
			"Jack",
			"Jackson",
			"Jacob",
			"James",
			"Jason",
			"Jayden",
			"John",
			"Jonah",
			"Jordan",
			"Joshua",
			"Justin",
			"Kevin",
			"Kyle",
			"Liam",
			"Logan",
			"Louis",
			"Luke",
			"Mason",
			"Matthew",
			"Max",
			"Michael",
			"Nathan",
			"Niall",
			"Noah",
			"Ryan",
			"Shawn",
			"Spencer",
			"Tyler",
			"William",
			"Zayn",
		};

		
		public List<String> getNames(Predicate<String> p) {
			List<String> rNames = new ArrayList<String>();
			
			for ( String n : NAMES )
				if ( p.test(n) )
					rNames.add(n);
			
			return rNames;
		}
	}

}
