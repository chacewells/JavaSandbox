package com.wellsfromwales.sandbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;


public class MergeEncode {
	
	static final String RESERVED_REGEX = "[\\Q:/?#[]@!$()+,;=%\\E]";
	static int count = 0;

	public static void main(String[] args) {
		Map<String,String> map = new HashMap<String, String>();
		map.put("hello","world");
		map.put("lucky", "dog");
		map.put("question", "answer");
		
		System.out.println(
				encode(NameValuePair.getList(map))
				);
	}
	
	static String encodeQueryPairs( List<NameValuePair> pairs ) {
		return StringUtils.join(pairs, '&');
	}
	
	static List<NameValuePair> encode( List<NameValuePair> pairs ) {
		List<NameValuePair> encoded = new ArrayList<NameValuePair>(pairs.size());
		pairs.stream()
		.forEach(pair -> encoded.add( encode(pair) ) );
		return encoded;
	}
	
	static NameValuePair encode( NameValuePair pair ) {
		return new NameValuePair( encode( pair.getName() ), encode( pair.getValue() ) );
	}
	
	static String encodeStream( String str ) {
		StringBuilder sb = new StringBuilder();
		List<Character> cList = Arrays.asList( ArrayUtils.toObject(str.toCharArray()));
		cList.stream()
		.forEach(c -> {sb.append( encode(c) );count++;} ); 
		
		return sb.toString();
	}
	
	static String encodeNormal( String str ) {
		StringBuilder sb = new StringBuilder();
		char[] charray = str.toCharArray();
		for ( char c : charray ) {
			sb.append( encode(c) );
			count++;
		}
		
		return sb.toString();
	}
	
	static String encode( String str ) {
		count++;
		if ( str.equals("") )
			return str;
		if ( str.length() == 1 )
			return encode( str.charAt(0) );
		
		int length = str.length();
		int middle = length / 2;
		
		return encode( str.substring(0, middle) ) + encode( str.substring(middle) );
	}
	
	static String encode( char c ) {
		if ( c == ' ' )
			return "+";
		else if ( (c+"").matches(RESERVED_REGEX) || c < 0x20 || c >= 0x7F )
			return String.format( "%c%02x", '%', Integer.valueOf(c) );
		else
			return "" + c;
	}

}
