package com.wellsfromwales.sandbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class PersonCase {
	
	public static final String[] BOY_NAMES = {
		"Aaron",
		"Aaron",
		"Adam",
		"Aiden",
		"Alex",
		"Alexander",
		"Andrew",
		"Anthony",
		"Austin",
		"Benjamin",
		"Blake",
		"Brandon",
		"Brian",
		"Christopher",
		"Daniel",
		"David",
		"Dylan",
		"Dylan",
		"Ethan",
		"Harry",
		"Ian",
		"Isaac",
		"Isaac",
		"Jack",
		"Jackson",
		"Jacob",
		"James",
		"James",
		"Jason",
		"Jayden",
		"John",
		"Jonah",
		"Jordan",
		"Joshua",
		"Joshua",
		"Justin",
		"Kevin",
		"Kyle",
		"Liam",
		"Logan",
		"Louis",
		"Luke",
		"Luke",
		"Mason",
		"Matthew",
		"Max",
		"Michael",
		"Nathan",
		"Niall",
		"Noah",
		"Noah",
		"Ryan",
		"Shawn",
		"Spencer",
		"Tyler",
		"William",
		"William",
		"Zayn",
	};
	
	public static final String[] GIRL_NAMES = {
		"Aaliyah",
		"Abby",
		"Abigail",
		"Alexis",
		"Alice",
		"Alyssa",
		"Amanda",
		"Amber",
		"Amy",
		"Anna",
		"Ashley",
		"Ava",
		"Bella",
		"Charlotte",
		"Chloe",
		"Elizabeth",
		"Ella",
		"Ellie",
		"Emily",
		"Emma",
		"Grace",
		"Hannah",
		"Isabella",
		"Jade",
		"Jasmine",
		"Jennifer",
		"Jessica",
		"Katie",
		"Lauren",
		"Lexi",
		"Lilly",
		"Lily",
		"Lucy",
		"Madison",
		"Megan",
		"Mia",
		"Natalie",
		"Nicole",
		"Olivia",
		"Paige",
		"Rachel",
		"Rebecca",
		"Samantha",
		"Sarah",
		"Savannah",
		"Sophia",
		"Sophie",
		"Taylor",
		"Vanessa",
		"Zoe",
	};

	interface TriFunction<T,U,V,Out> {
		public Out apply(T t, U u, V v);
	}
	
	static List<Person> getPeople() {
		TriFunction<String,Integer,Person.Gender,Person> tf = (n,a,g) -> (new Person()).setFirst(n).setAge(a).setGender(g);
		
		Random rand = new Random(50);
		List<Person> people = new ArrayList<Person>(BOY_NAMES.length + GIRL_NAMES.length);
		
		Arrays.stream( ArrayUtils.addAll(BOY_NAMES, GIRL_NAMES) )
		.forEach(name -> {
			people.add( tf.apply( name, rand.nextInt(70),
					ArrayUtils.contains(BOY_NAMES, name)
						? Person.Gender.MALE 
								: Person.Gender.FEMALE ) );
		});
		
		Collections.shuffle(people);
		return people;
	}
	
	public static void main(String[] args) {
		List<Person> peeps = getPeople();
		peeps.stream()
		.filter(p -> p.getGender() == Person.Gender.FEMALE )
		.sorted( (p1,p2) -> p2.getAge() - p1.getAge() )
		.forEach( System.out::println );
	}
	
	private static class Person {
		String first;
		int age;
		Gender gender;
		enum Gender { MALE, FEMALE };
		public String getFirst() {
			return first;
		}
		public Gender getGender() {
			return gender;
		}
		public Person setGender(Gender gender) {
			this.gender = gender;
			return this;
		}
		public Person setFirst(String first) {
			this.first = first;
			return this;
		}
		public int getAge() {
			return age;
		}
		public Person setAge(int age) {
			this.age = age;
			return this;
		}
		@Override
		public String toString() {
			int age = getAge();
			return (new StringBuilder())
					.append(StringUtils.rightPad(getFirst() + ": ", 15))
					.append(StringUtils.leftPad(getAge() + " year" + (age==1?"":'s') + " old", 15))
					.toString();
		}
	}

}
