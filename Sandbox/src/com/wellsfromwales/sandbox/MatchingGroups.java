package com.wellsfromwales.sandbox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchingGroups {
	
	static final String URL = "http3://www.dum-my.com:81/path/to/file?query=string#frag";
	static final String PROTOCOL = "^((?<protocol>[A-Za-z0-9]+)://)?",
						DOMAIN = "((^|://)(?<domain>([A-Za-z0-9-]+\\.)*[A-Za-z0-9-]+)([:/]|$))?",
						PORT = "(:(?<port>\\d+)(/|$))",
						PATH = "([^/]/{1}(?<path>((\\w\\.)*\\w+/?)+))",
						QUERY = "(/?\\?(?<query>(\\w+\\=\\w+&?)+))",
						FRAGMENT_ID = "(/?#(?<fragment>.+))";

	public static void main(String[] args) {
		System.out.println( decodeProtocol() );
		System.out.println( decodeDomain() );
		System.out.println( decodePort() );
		System.out.println( decodePath() );
		System.out.println( decodeQuery() );
		System.out.println( decodeFragmentID() );
		Pattern pat = Pattern.compile("(?<label>[Ww])\\k<label>*?");
		Matcher mat = pat.matcher(URL);
		if (mat.find())
			System.out.println(mat.group());
	}
	
	public static String decodeProtocol() {
		Pattern pattern = Pattern.compile(PROTOCOL);
		Matcher matcher = pattern.matcher(URL);
		matcher.find();
		return matcher.group("protocol");
	}
	
	public static String decodeDomain() {
		String[] splitted = URL.split("://", 2);
		String url = (splitted.length > 1) ? splitted[1] : splitted[0];
		Pattern pattern = Pattern.compile(DOMAIN);
		Matcher matcher = pattern.matcher(url);
		String domain = matcher.find() ? matcher.group("domain") : null;
		
		return domain;
	}
	
	public static int decodePort() {
		Pattern pattern = Pattern.compile(PORT);
		Matcher matcher = pattern.matcher(URL);
		String sPort = matcher.find() ? matcher.group("port") : null;
		int port = (sPort != null) ? Integer.parseInt(sPort) : -1;
		
		return port;
	}
	
	public static String decodePath() {
		Pattern pattern = Pattern.compile(PATH);
		Matcher matcher = pattern.matcher(URL);
		String path = matcher.find() ? matcher.group("path") : null;
		
		return path;
	}
	
	public static String decodeQuery() {
		Pattern pattern = Pattern.compile(QUERY);
		Matcher matcher = pattern.matcher(URL);
		String query = matcher.find() ? matcher.group("query") : null;
		
		return query;
	}
	
	public static String decodeFragmentID() {
		Pattern pattern = Pattern.compile(FRAGMENT_ID);
		Matcher matcher = pattern.matcher(URL);
		String fragmentID = matcher.find() ? matcher.group("fragment") : null;
		
		return fragmentID;
	}

}
