package com.wellsfromwales.sandbox;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

public class StreamEncode {
	
	public static void main(String[] args) {
		System.out.println( encode("%as the wind blows! @ your favorite farmer supply store :)") );
	}
	
	static String encode( String str ) {
		StringBuilder sb = new StringBuilder();
		
		Arrays.asList( ArrayUtils.toObject( str.toCharArray() ) )
		.stream()
		.forEach( c -> {
			if (c == ' ') sb.append("+");
			else if ((""+c).matches(MergeEncode.RESERVED_REGEX) || c < 0x20 || c >= 0x7F)
				sb.append( String.format( "%c%02x",'%',Integer.valueOf(c) ) );
			else sb.append(c);
		});
		
		return sb.toString();
	}
	
	static String encodeNormal( String str ) {
		StringBuilder sb = new StringBuilder();
		char[] charray = str.toCharArray();
		for ( char c : charray ) {
			if (c == ' ') sb.append("+");
			else if ((""+c).matches(MergeEncode.RESERVED_REGEX) || c < 0x20 || c >= 0x7F)
				sb.append( String.format( "%c%02x",'%',Integer.valueOf(c) ) );
			else sb.append(c);
		}
		
		return sb.toString();
	}
}
