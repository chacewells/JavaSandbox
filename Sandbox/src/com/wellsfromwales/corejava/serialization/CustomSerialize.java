package com.wellsfromwales.corejava.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class CustomSerialize {

	public static void main(String[] args) 
			throws FileNotFoundException, IOException, ClassNotFoundException {
		DataBucket db = new DataBucket("Bucket 'o Data", new DataPoint(15, 2000));
		
		String filename = "data_bucket.dat";
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
		out.writeObject(db);
		out.close();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
		db = (DataBucket)in.readObject();
		System.out.println(db);
		in.close();
	}
	
	static class DataPoint {
		int x;
		int y;
		public DataPoint(int x, int y) {this.x = x; this.y = y;}
		
		@Override
		public String toString() {
			return "" + x + ',' + y;
		}
	}
	
	static class DataBucket implements Serializable {
		String name;
		transient DataPoint dp;
		
		public DataBucket(String name, DataPoint dp){this.name = name; this.dp = dp;}
		
		private void writeObject(ObjectOutputStream out)
				throws IOException
		{
			out.defaultWriteObject();
			out.writeInt(dp.x);
			out.writeInt(dp.y);
		}
		
		private void readObject(ObjectInputStream in)
				throws IOException, ClassNotFoundException
		{
			in.defaultReadObject();
			int x = in.readInt();
			int y = in.readInt();
			dp = new DataPoint(x, y);
		}

		@Override
		public String toString() {
			return "DataBucket [name=" + name + ", dp=" + dp + "]";
		}
		
	}

}
