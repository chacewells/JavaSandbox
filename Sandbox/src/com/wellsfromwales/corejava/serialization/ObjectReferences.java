package com.wellsfromwales.corejava.serialization;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ObjectReferences {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String filename = "people.dat";
		
		Person aaron = new Person();
		aaron.firstName = "Aaron";
		aaron.lastName = "Wells";
		aaron.gender = Person.Gender.MALE;
		
		Person jenni = new Person();
		jenni.firstName = "Jenni";
		jenni.lastName = "Wells";
		jenni.gender = Person.Gender.FEMALE;
		
		aaron.spouse = jenni;
		jenni.spouse = aaron;
		
		Person doug = new Person();
		Person valerie = new Person();
		
		doug.firstName = "Doug";
		doug.lastName = "Wells";
		doug.gender = Person.Gender.MALE;
		doug.spouse = valerie;
		
		valerie.firstName = "Valerie";
		valerie.lastName = "Wells";
		valerie.gender = Person.Gender.FEMALE;
		valerie.spouse = doug;
		
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
		for (Person p : new Person[]{aaron,jenni,doug,valerie})
			out.writeObject(p);
		out.close();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
		Person p = null;
		try {
			while (true) {
				p = (Person)in.readObject();
				System.out.println(p);
			}
		} catch ( EOFException e ) {
			if ( in != null)
				in.close();
		}
	}
	
	static class Person implements Serializable {
		String firstName;
		String lastName;
		Person spouse;
		enum Gender {MALE,FEMALE}
		Gender gender;
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName
					+ ", spouse=" + spouse.firstName + ", gender=" + gender + "]";
		}
		
	}

}
