package com.wellsfromwales.corejava.serialization;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializeTypesafeAndSingleton {

	public static void main(String[] args) 
			throws FileNotFoundException, IOException, ClassNotFoundException {
		AwesomeConstant c1 = AwesomeConstant.FIRST_CONSTANT;
		AwesomeConstant c2 = AwesomeConstant.SECOND_CONSTANT;
		AwesomeConstant c3 = AwesomeConstant.THIRD_CONSTANT;
		
		String filename = "awesome_constants.dat";
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
		out.writeObject(c1);
		out.writeObject(c2);
		out.writeObject(c3);
		out.close();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
		try {
				AwesomeConstant d1 = (AwesomeConstant)in.readObject();
				System.out.println(d1 == c1);
				AwesomeConstant d2 = (AwesomeConstant)in.readObject();
				System.out.println(d2 == c2);
				AwesomeConstant d3 = (AwesomeConstant)in.readObject();
				System.out.println(d3 == c3);
		} catch (EOFException e) {
			if (in != null) in.close();
		}
	}
	
	static class GreatSingleton implements Serializable {
		private static GreatSingleton unique;
		double data;
		private GreatSingleton(double d) {
			data = d;
		}
		public static GreatSingleton getInstance(double d) {
			if (unique == null)
				unique = new GreatSingleton(d);
			return unique;
		}
		
	}
	
	static class AwesomeConstant implements Serializable {
		public static final AwesomeConstant FIRST_CONSTANT = new AwesomeConstant(0);
		public static final AwesomeConstant SECOND_CONSTANT = new AwesomeConstant(1);
		public static final AwesomeConstant THIRD_CONSTANT = new AwesomeConstant(2);
		
		public int data;
		
		private AwesomeConstant( int data ) {
			this.data = data;
		}
		
		protected Object readResolve() {
			if (data == 0) return FIRST_CONSTANT;
			if (data == 1) return SECOND_CONSTANT;
			if (data == 2) return THIRD_CONSTANT;
			return null;	//	this should never happen
		}
		
		@Override
		public String toString() {
			if (data == 0) return "FIRST_CONSTANT";
			if (data == 1) return "SECOND_CONSTANT";
			if (data == 2) return "THIRD_CONSTANT";
			return null;	//	this should never happen
		}
	}

}
