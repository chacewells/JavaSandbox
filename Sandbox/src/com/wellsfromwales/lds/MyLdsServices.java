package com.wellsfromwales.lds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MyLdsServices {

	public static void main(String[] args) throws IOException {
		URL url = new URL("https://signin.lds.org/login.html");
		HttpURLConnection cxn = (HttpsURLConnection) url.openConnection();
		cxn.setRequestMethod("POST");
		cxn.setRequestProperty("username", "chacewells");
		cxn.setRequestProperty("password", "");
		cxn.connect();
		InputStreamReader reader = new InputStreamReader(cxn.getInputStream());
		BufferedReader bReader = new BufferedReader(reader);
		
		String line;
		while ( (line = bReader.readLine()) != null ) {
			System.out.println(line);
		}
	}

}
