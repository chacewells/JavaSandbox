package com.wellsfromwales.beans;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PropertyFile {
	
	private String property1;
	private String property2;
	
	public PropertyFile() {
		this.property1 = "";
		this.property2 = "";
	}
	
	public PropertyFile(String property1, String property2) {
		this.property1 = property1;
		this.property2 = property2;
	}
	
	public void setProperty1(String property1) {
		this.property1 = property1;
	}
	
	public String getProperty1() {
		return property1;
	}
	
	public String getProperty2() {
		return property2;
	}
	
	public void setProperty2(String property2) {
		this.property2 = property2;
	}
	
	@Override
	public String toString() {
		return "Properties: " + getProperty1() + " " + getProperty2();
	}

	public static void main(String[] args) throws FileNotFoundException {
		try (XMLEncoder out = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(new File("prop_file.xml"))))) {
			out.writeObject(new PropertyFile("prop 1", "prop 2"));
		}
		
		try (XMLDecoder in = new XMLDecoder(new BufferedInputStream(new FileInputStream(new File("prop_file.xml"	))))) {
			PropertyFile propFile = (PropertyFile) in.readObject();
			System.out.println(propFile);
		}
	}

}
