package com.wellsfromwales.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListComparisons {

	static final String[] strings = { "dog", "cat", "mouse", "hat", "bat",
			"dimes", "dollars", "cookie", "jar", "onerous", "specific" };

	public static void main(String[] args) {
		List<String> arrayL = new ArrayList<>();
		List<String> linkedL = new LinkedList<>();

		long start = System.nanoTime();
		for (String s : strings)
			arrayL.add(s);
		long end = System.nanoTime();
		System.out.println("ArrayList add time: " + (end - start));

		start = System.nanoTime();
		for (String s : strings)
			linkedL.add(s);
		end = System.nanoTime();
		System.out.println("LinkedList add time: " + (end - start));

		start = System.nanoTime();
		for (String s : strings)
			arrayL.add(5, s);
		end = System.nanoTime();
		System.out.println("ArrayList add to middle: " + (end - start));

		start = System.nanoTime();
		for (String s : strings)
			linkedL.add(5, s);
		end = System.nanoTime();
		System.out.println("LinkedList add to middle: " + (end - start));

		start = System.nanoTime();
		for (int i = 0; i < strings.length; i++)
			arrayL.size();
		end = System.nanoTime();
		System.out.println("ArrayList size time: " + (end - start));

		start = System.nanoTime();
		for (int i =0; i < strings.length; i++)
			linkedL.size();
		end = System.nanoTime();
		System.out.println("LinkedList size time: " + (end - start));
	}

}
