package com.wellsfromwales.export;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Stack;
import java.util.StringTokenizer;

public class CarriersOnASL {

	public static void main(String[] args) throws IOException {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the carriers sailing from origin (names enclosed in double-quotations)");
		System.out.flush();
		Collection<String> sailing = parseArgs(keyboard.readLine());
		System.out.println("enter the carriers listed on the ASL (names enclosed in double-quotations)");
		System.out.flush();
		Collection<String> asl= parseArgs(keyboard.readLine());
		asl.retainAll(sailing);
		System.out.println("here are the ASL carriers sailing this leg:");
		for (String carrier : asl)
			System.out.print("'" + carrier + "'" + "\t");
		
		keyboard.close();
	}
	
	public static Collection<String> parseArgs(String argString) {
		StringTokenizer st = new StringTokenizer(argString, "\"", true);
		Collection<String> result = new HashSet<>();
		Stack<String> quotes = new Stack<>();
		StringBuilder sb = new StringBuilder();
		
		while (st.hasMoreElements()) {
			String token = st.nextToken();
			if (token.equals("\"")) {
				quotes.push(token);
				while (!quotes.empty()) {
					if ((token = st.nextToken()).equals("\""))
						quotes.pop();
					else
						sb.append(token.toUpperCase());
				}
			}
			if (sb.length() > 0) result.add(sb.toString());
			sb.setLength(0);
		}
		
		return result;
	}

}
