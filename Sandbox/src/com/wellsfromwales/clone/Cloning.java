package com.wellsfromwales.clone;

public class Cloning {

	public static void main(String[] args) throws CloneNotSupportedException {
		ContainerShallow shallow = new ContainerShallow("shallow", "container");
		ContainerShallow hal = (ContainerShallow) shallow.clone();
		System.out.println("before mod:");
		System.out.println("shallow: " + shallow);
		System.out.println("hal: " + hal);
		System.out.println();
		hal.ref.one = "marshmallow";
		System.out.println("after mod:");
		System.out.println("shallow: " + shallow);
		System.out.println("hal: " + hal);
		System.out.println();
		
		ContainerDeep deep = new ContainerDeep("deep", "container");
		ContainerDeep blue = (ContainerDeep) deep.clone();
		System.out.println("before mod:");
		System.out.println("deep: " + deep);
		System.out.println("blue: " + blue);
		System.out.println();
		blue.ref.one = "no change in deep";
		System.out.println("after mod:");
		System.out.println("deep: " + deep);
		System.out.println("blue: " + blue);
	}
	
	static class ReferenceObject implements Cloneable {
		String one;
		String two;
		
		@Override
		public Object clone() throws CloneNotSupportedException {
			return super.clone();
		}

		@Override
		public String toString() {
			return "ReferenceObject [one=" + one + ", two=" + two + "]";
		}
	}
	
	static abstract class AbstractContainer implements Cloneable {
		ReferenceObject ref;
		public AbstractContainer(String one, String two) {
			ref = new ReferenceObject();
			ref.one = one;
			ref.two = two;
		}

		@Override
		public String toString() {
			return "ContainerObjectShallow [ref=" + ref + "]";
		}
	}
	
	static class ContainerShallow extends AbstractContainer {
		public ContainerShallow(String one, String two) {
			super(one,two);
		}
		
		@Override
		public Object clone() throws CloneNotSupportedException {
			return super.clone();
		}
	}
	
	static class ContainerDeep extends AbstractContainer {
		public ContainerDeep(String one, String two) {
			super(one,two);
		}
		
		@Override
		public Object clone() throws CloneNotSupportedException {
			ContainerDeep o = (ContainerDeep)super.clone();
			o.ref = new ReferenceObject();
			o.ref.one = ref.one;
			o.ref.two = ref.two;
			return (Object) o;
		}
	}

}
